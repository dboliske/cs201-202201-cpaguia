package exams.first;

import java.util.Scanner;

public class QuestionFour {

	public static void main(String[] args) {
		// Arrays

		//take in 5 user inputs (for loop), uses a for double for loop to check if any of them equal each other (without counting itself),
		//print if there are two of the same words
		
		Scanner input = new Scanner(System.in); //create scanner object
		String[] str = new String[5];
		for (int i=1; i<=5; i++) {
			System.out.println("Word number " + i + ":");
			str[i-1] = input.nextLine();
		}
		
		boolean copy = false; //flag if strings match
		for (int i=1; i<=5; i++) {  //start on first entry
			for (int i1=i; i1<5; i++) { //loop through other entries
				if (str[i-1].equalsIgnoreCase(str[i1-1])) {//check whether first entry compares to other entries
					copy = true;
				}
			}
		}
		if (copy) { //if string match is detected. print
			System.out.println("There are two strings that match");
		}
		
		
	}

}
