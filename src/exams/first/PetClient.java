package exams.first;

public class PetClient {

	public static void main(String[] args) {
		// Pet Client
		
		Pet p = new Pet();
		
		System.out.println(p.getName());
		System.out.println(p.getAge());
		System.out.println(p.toString());
		p.setName("Sparky");
		p.setAge(5);
		System.out.println(p.toString());

	}

}
