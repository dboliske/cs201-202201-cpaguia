package exams.first;

import java.util.Scanner;

public class QuestionTwo {

	public static void main(String[] args) {
		// Selection
		
		Scanner input = new Scanner(System.in); //create scanner object
		
		int num = 0;
		boolean done = false;
		while (!done) {
			try {
				System.out.println("Number: "); //prompt user for input
				num = Integer.parseInt(input.nextLine());
				done = true;
				break;
			} catch (Exception e) {
				System.out.println("Not an int");
			}
		}
		
		if ((num % 2 == 0) && (num % 3 == 0)) {
			System.out.println("foobar");
		} else if (num % 2 == 0) {
			System.out.println("foo");
		} else if (num % 3 == 0) {
			System.out.println("bar");
		} else {
			System.out.println("error");
		}
		
	}

}
