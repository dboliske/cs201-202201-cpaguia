package exams.first;

public class Pet {

	private String name;
	private int age;
	
	public Pet() {
		name = "Spot";
		age = 1;
	}
	
	public Pet(String name, int age) {
		name = "Spot";
		setName(name);
		age = 1;
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age>0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet p) {
		if (name.equalsIgnoreCase(p.getName()) && age == p.getAge()) {
			return true;
		} else {
			return false;
		}
	}
	
	public String toString() {
		return name + " is " + age + " years old";
	}
	
	
}
