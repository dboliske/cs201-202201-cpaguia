package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		// Repetition
		
		Scanner input = new Scanner(System.in); //create scanner object
		
		int num = 0;
		boolean running = true;
		while (running) {
			try {
				System.out.println("Number: "); //prompt user for input
				num = Integer.parseInt(input.nextLine());
				running = false;
				break;
			} catch (Exception e) {
				System.out.println("Not an int");
			}
		}
		
		for (int i=1; i<=num; i++) {
			for (int i1=0; i1<i; i1++) {
				System.out.print("*");
			}
			System.out.print("\n");
		}

	}

}
