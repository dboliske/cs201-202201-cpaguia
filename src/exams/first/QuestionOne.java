package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		// Data Types
		//Charles Paguia
		
		Scanner input = new Scanner(System.in); //create scanner object
				
		try {
			System.out.println("Number: "); //prompt user
			int number = input.nextInt(); //collect input
			
			int newNum = number + 65; //add 65 to number
			char character = (char) newNum; //cast int number to char
			
			System.out.println(character); //Output character
		} catch (Exception e) {
			System.out.println("Not an int");
		}
		
		System.out.println("Ending program");
		
	}

}
