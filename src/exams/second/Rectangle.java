package exams.second;

public class Rectangle extends Polygon {

	private double width;
	private double height;
	
	public Rectangle() {
		width = 1;
		height = 1;
	}

	public double getWidth() {
		return width;
	}

	//make sure width is positive
	public void setWidth(double width) {
		if (width > 0) {
			this.width = width;
		}
	}

	public double getHeight() {
		return height;
	}

	//make sure height is positive
	public void setHeight(double height) {
		if (height > 0) {
			this.height = height;
		}
	}
	
	@Override
	public String toString() {
		return super.toString() + "Rectangle [width=" + width + ", height=" + height + "]";
	}
	
	//define abstract classes from polygon class
	public double area() {
		double area = height * width;
		return area;
	}
	
	public double perimeter() {
		double perimeter = 2.0 * (height + width);
		return perimeter;
	}
	
}
