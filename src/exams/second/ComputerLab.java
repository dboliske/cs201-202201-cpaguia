package exams.second;

public class ComputerLab extends Classroom {

	private boolean computers;
	
	public ComputerLab() {
		this.computers = true;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}

	@Override
	public String toString() {
		return super.toString() + " and " + ( hasComputers() ?" has " : " does not have ") + "computers.";
	}
	
	public static void main(String[] args) {
		ComputerLab lab = new ComputerLab();
		System.out.println(lab.toString());
	}
	
}
