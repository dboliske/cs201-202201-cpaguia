package exams.second;

public abstract class Polygon {

	protected String name;
	
	public Polygon() {
		name = "polygon";
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "Name = " + name + ". ";
	}
	
	//declare abstract methods
	public abstract double area();
	public abstract double perimeter();
	
}
