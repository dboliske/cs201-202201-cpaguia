package exams.second;

import java.util.Scanner;

public class Searching {
	
	public static int startSearch(double[] array, double search) {
		//initialize step and checkpoint
		int step = (int)Math.sqrt(array.length);
		int checkpoint = 0;
		
		return jumpSearch(array, search, step, checkpoint);
	}
	
	
	
	public static int jumpSearch(double[] array, double search, int step, int checkpoint) {
		if (step < array.length-1 && array[step] < search) {
			checkpoint = step;
			step += step;
			return jumpSearch(array, search, step, checkpoint);
		} else {
			if (array[checkpoint] != search && checkpoint < array.length - 1 && checkpoint < step) {
				checkpoint++;
				return jumpSearch(array, search, step, checkpoint);
			} else if (array[checkpoint] == search) {
				return checkpoint;
			} else {
				return -1;
			}
		}
	}

	public static void main(String[] args) {
		//array, and input, and search variable
		double[] array = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		Scanner input = new Scanner(System.in);
		double search = 0;
		
		//prompt user
		System.out.println("Search for: ");
		try {
			search = Double.parseDouble(input.nextLine());
		} catch (NumberFormatException nfe) {
			System.out.println("Error.");
		}
		//search
		double found = startSearch(array, search);

		System.out.print(search + " found at index " + ((found != -1) ? found : "NA" ));
	}

	

}
