package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {	
	
	public static void main(String[] args) {
		//initializes scanner array and loop flag
		boolean running = true;
		Scanner input = new Scanner(System.in);
		ArrayList<Integer> list = new ArrayList<Integer>(10);
		
		do {
			//repeat menu until invalid entry
			System.out.println("Number (Enter 'Done' to exit): ");
			try {
				//get user input as string
				String entry = input.nextLine();
				//if user enters 'done' exit program
				if (entry.equalsIgnoreCase("done")) {
					System.out.println("Exitting.");
					running = false;
				} else {
					//if not 'done' try to parse to int and add to list
					int num = Integer.parseInt(entry);
					list.add(num);
				}
			//if entry cant be parsed to int try-catch will catch it.
			} catch (NumberFormatException nfe) {
				System.out.println("Invalid entry.");
			}
		} while (running);
		input.close();
		//set max and mins to highest and lowest possible so that anything
		//that is entered must be higher or lower.
		int max = -2147483647;
		int min = 2147483647;
		//iterate over array
		for (int e : list) {
			//find min
			if (e < min) {
				min = e;
			//find max
			}
			if (e > max) {
				max = e;
			}
		}
		
		//disp max and min
		System.out.println("Max: " + ((max==0) ? "NA" : max) );
		System.out.println("Min: " + ((min==0) ? "NA" : min) );
	}

}
