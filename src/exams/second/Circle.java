package exams.second;

public class Circle extends Polygon {

	private double radius;
	
	public Circle() {
		radius = 1;
	}

	public double getRadius() {
		return radius;
	}

	//make sure radius is positive
	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		}
	}
	
	@Override
	public String toString() {
		return super.toString() + "Circle [radius=" + radius + "]";
	}
	
	//define abstract classes from polygon class
	public double area() {
		double area = Math.PI * radius * radius;
		return area;
	}
	
	public double perimeter() {
		double perimeter = 2.0 * Math.PI * radius;
		return perimeter;
	}
	
}
