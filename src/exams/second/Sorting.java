package exams.second;

public class Sorting {
	
	public static String[] selectionSort(String[] list) {
		
		//for each element find the min
		for (int i = 0; i < list.length ; i++) {
			int min = i;
			//find the min excluding the sorted.
			for (int j = i+1 ; j < list.length ; j++) {
				if (list[j].compareToIgnoreCase(list[min]) < 0) {
					min = j;
				}
			}
			//if min: swap
			if (min != i) {
				String temp = list[i];
				list[i] = list[min];
				list[min] = temp;
			}
		}
		//return sorted list
		return list;
	}

	public static void main(String[] args) {
		String[] list = {"speaker", "poem", "passenger", "tale", "reflection", 
				"leader", "quality", "percentage", "height",
				"wealth", "resource", "lake", "importance"};
		list = selectionSort(list);	
		//display sorted list
		for (String s : list) {
			System.out.print(s + ", ");
		}
	}
}
