package exams.second;

public class Classroom {

	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		this.building = "Wishnick";
		this.roomNumber = "210A";
		this.seats = 25;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if (seats > 0) {
			this.seats = seats;
		}
	}
	
	@Override
	public String toString() {
		return "Room " + roomNumber + " in " + building + " has " + seats + " seats";
	}
	
}
