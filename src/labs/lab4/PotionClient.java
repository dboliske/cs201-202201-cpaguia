package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		// Potion Client

		Potion p1 = new Potion();
		
		System.out.println(p1.toString()); //check defaults
		p1.setName("power"); //check 
		p1.setStrength(7.5); //set to valid
		p1.setStrength(20); //try to set to invalid
		System.out.println(p1.toString()); //check if it was set to invalid
		System.out.println(p1.validStrength(20)); //check validstrength
		System.out.println(p1.validStrength()); //check valid strength
		System.out.println(p1.getName()); //check getters
		System.out.println(p1.getStrength());

		
		Potion p2 = new Potion();
		
		p2.setName("Brightness");
		p2.setStrength(2.5);
		System.out.println(p2.toString());

		Potion p3 = new Potion();
		
		p3.setName("POWER");
		p3.setStrength(7.5);
		
		System.out.println(p1.equals(p2));
		System.out.println(p1.equals(p3));
		System.out.println(p2.equals(p3));
		
	}

}
