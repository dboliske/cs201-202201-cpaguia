package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		// PhoneNumber Client

		PhoneNumber pn1 = new PhoneNumber();
		
		//check tostring, constructors, and setters
		System.out.println(pn1.toString()); //print default
		pn1.setCountryCode("20"); //try setting to invalid cC
		System.out.println(pn1.toString()); //check to see if it set to invalid cC
		pn1.setCountryCode("2"); //set to valid cC
		System.out.println(pn1.toString());
		pn1.setAreaCode("5"); //set to invalid aC
		System.out.println(pn1.toString());
		pn1.setAreaCode("219"); //set to valid aC
		System.out.println(pn1.toString());
		pn1.setAreaCode("2192"); //set to invalid aC
		System.out.println(pn1.toString());
		pn1.setNumber("440923"); //set to invalid num (not enough numbers)
		System.out.println(pn1.toString());
		pn1.setNumber("44092a3"); //set to invalid num (not a number)
		System.out.println(pn1.toString());
		pn1.setNumber("4409273"); //set to valid num
		System.out.println(pn1.toString());
		//check getters
		System.out.println(pn1.getCountryCode());
		System.out.println(pn1.getAreaCode());
		System.out.println(pn1.getNumber());
		//check validations
		System.out.println(pn1.validCountryCode("3020")); //false
		System.out.println(pn1.validCountryCode("3")); //true
		System.out.println(pn1.validCountryCode()); //true
		System.out.println(pn1.validAreaCode("54721")); //false
		System.out.println(pn1.validAreaCode("302")); //true
		System.out.println(pn1.validAreaCode()); //true
		System.out.println(pn1.validNumber("102a459")); //false
		System.out.println(pn1.validNumber("4302930")); //true
		System.out.println(pn1.validNumber()); //true
		
		PhoneNumber pn2 = new PhoneNumber();
		
		pn2.setCountryCode("4");
		pn2.setAreaCode("604");
		pn2.setNumber("1234567");
		
		PhoneNumber pn3 = new PhoneNumber();
		
		pn3.setCountryCode("2");
		pn3.setAreaCode("219");
		pn3.setNumber("4409273");
		
		System.out.println(pn1.equals(pn2)); //false
		System.out.println(pn2.equals(pn3)); //false
		System.out.println(pn1.equals(pn3)); //true
		
	}

}
