package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		// GeoLocation Client

		GeoLocation g1 = new GeoLocation();	//geolocation instance 1
		
		System.out.println(g1.toString());
		System.out.println(g1.getLat());
		System.out.println(g1.getLng());
		System.out.println(g1.validLat());
				
		GeoLocation g2 = new GeoLocation(); //geolocation instance 2
		
		g2.setLat(200.2); //try setting to impossible cord
		g2.setLng(200.4); //try setting to impossible cord
		g2.setLat(50.3); // set to possible cord
		g2.setLng(29.76); //set to possible cord
		System.out.println(g2.getLat()); //get lat and lng of geolocation instance 2 and check get methods
		System.out.println(g2.getLng());
		System.out.println(g2.toString());
		System.out.println(g2.validLat()); //check valid method
		
		GeoLocation g3 = new GeoLocation(); //geolocation instance 3
		
		g3.setLat(402.5); //try setting to impossible coord
		g3.setLng(203); //try setting to impossible coord
		System.out.println(g3.toString()); //print g3 tostring
		System.out.println(g3.validLat(500)); //check impossible coord
		System.out.println(g3.validLng(200)); //check impossible coord
		
		
		//equals statements
		System.out.println(g1.equals(g2));
		System.out.println(g1.equals(g3));
		System.out.println(g2.equals(g3));
		
		System.out.println(g1.calcDistance(40.2, 30.3));
		System.out.println(g1.calcDistance(g2));
		
	}

}
