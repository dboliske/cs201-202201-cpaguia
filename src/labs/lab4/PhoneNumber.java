package labs.lab4;

public class PhoneNumber {

	private String countryCode;
	private String areaCode;
	private String number;
	
// constructors --------------------------------------------------------------------------------------
	
	public PhoneNumber() { //initialize PhoneNumber
		this.countryCode = "1";
		this.areaCode = "872";
		this.number = "2222222";
	}
	
	public PhoneNumber(String countryCode, String areaCode, String number) { //set default phonenumber
		this.countryCode = "1";
		setCountryCode(countryCode);
		this.areaCode = "872";
		setAreaCode(areaCode);
		this.number = "2222222";
		setNumber(number);
	}
	
// getters -------------------------------------------------------------------------------------------	
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
// setters -------------------------------------------------------------------------------------------
	
	public void setCountryCode(String countryCode) {
		if (validCountryCode(countryCode)) { //check whether it is valid, then set it
			this.countryCode = countryCode;
		}
	}
	
	public void setAreaCode(String areaCode) {
		if (validAreaCode(areaCode)) {
			this.areaCode = areaCode;
		}
	}
	
	public void setNumber(String number) {
		if (validNumber(number)) {
			this.number = number;
		}
	}
	
	public String toString() { //toString: add dashes between numbers
		return "Phone number: +" + this.countryCode + "-" + this.areaCode + "-" + this.number;
	}
	
// validations ---------------------------------------------------------------------------------------
	
	public boolean validCountryCode(String countryCode) {
		if (countryCode.length() != 1) {
			return false;
		} else if (countryCode.charAt(0) >= 48 && countryCode.charAt(0) <= 57) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validAreaCode(String areaCode) {
		boolean check = false; //check flag (default false)
		if (areaCode.length() != 3) { //check length of string first
			return false;
		} else {
			for (char c : areaCode.toCharArray()) { //take each char of string and check if its a valid number
				if (c >= 48 && c <= 57) {
					check = true;
				} else {
					return false; //as soon as there is not a valid number validation is deemed false
				}
			}
		}
		return check;
	}
	
	public boolean validNumber(String number) {
		boolean check = false; //check flag (default false)
		if (number.length() != 7) { //check length of string first
			return false;
		} else {
			for (char c : number.toCharArray()) { //take each char of string and check if its a valid number
				if (c >= 48 && c <= 57) {
					check = true;
				} else {
					return false; //as soon as there is not a valid number validation is deemed false
				}
			}
		}
		return check;
	}
	
// empty parameter validation to check the instance variable ---------------------------------------
	
	public boolean validCountryCode() {
		if (countryCode.length() != 1) {
			return false;
		} else if (countryCode.charAt(0) >= 48 && countryCode.charAt(0) <= 57) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validAreaCode() {
		boolean check = false; //check flag (default false)
		if (areaCode.length() != 3) { //check length of string first
			return false;
		} else {
			for (char c : areaCode.toCharArray()) { //take each char of string and check if its a valid number
				if (c >= 48 && c <= 57) {
					check = true;
				} else {
					return false; //as soon as there is not a valid number validation is deemed false
				}
			}
		}
		return check;
	}
	
	public boolean validNumber() {
		boolean check = false; //check flag (default false)
		if (number.length() != 7) { //check length of string first
			return false;
		} else {
			for (char c : number.toCharArray()) { //take each char of string and check if its a valid number
				if (c >= 48 && c <= 57) {
					check = true;
				} else {
					return false; //as soon as there is not a valid number validation is deemed false
				}
			}
		}
		return check;
	}
	
// equals method ------------------------------------------------------------------------------------
	
	public boolean equals(PhoneNumber pn) { //if cC equal, check aC equal, check num equal. else is false
		if (this.countryCode == pn.getCountryCode()) {
			if (this.areaCode == pn.getAreaCode()) {
				if (this.number == pn.getNumber()) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
