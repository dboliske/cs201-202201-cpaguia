package labs.lab4;

public class Potion {

	private String name;
	private double strength;
	
	public Potion() {
		name = "health";
		strength = 5.0;
	}
	
	public Potion(String name, double strength) {
		name = "health";
		setName(name);
		strength = 5.0;
		setStrength(strength);
	}
	
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		if (validStrength(strength)) {
			this.strength = strength;
		}
	}
	
	@Override
	public String toString() {
		return name.substring(0,1).toUpperCase() + name.substring(1) + " Potion: Strength = " + strength;
	}
	
	public boolean validStrength(double strength) {
		if (strength > 0 && strength <= 10) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validStrength() {
		if (this.strength > 0 && this.strength <= 10) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(Potion p) {
		if (name.equalsIgnoreCase(p.getName()) && strength == p.getStrength()) {
			return true;
		} else {
			return false;
		}
	}
	
}
