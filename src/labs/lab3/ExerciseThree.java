package labs.lab3;

public class ExerciseThree {

	public static void main(String[] args) {
		// Number minimum program, Charles Paguia, CS 201 Boliske, 2.8.2022
		// Goal: Calculate minimum value of numbers
		
		//initialize array with numbers
		int[] values = {
		72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 
		115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421
		};
		
		int min = values[0]; //initialize min
		for (int i=1; i<values.length; i++) { //if number is lower than min, it becomes new min
			min = (min < values[i]) ? min : values[i];
		}
		System.out.println("Min number" + min); //print min		
	}

}
