package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) throws IOException {
		// File writing program, Charles Paguia, CS 201 Boliske, 1.25.2022
		// Goal: Ask user for indefinite amount of numbers. Write to specified file.
		
		Scanner input = new Scanner(System.in); //initialize scanner object
		boolean running = true; //initialize loop flag
		int counter = 0; //create counter to count how many times loop has run
		
		int values[] = new int[1];
		while(running) {
			try {
				System.out.println("Number " + (counter + 1) + ": (Enter -1 to stop)"); //prompt user for number
				int num = Integer.parseInt(input.nextLine());
				if (num == -1) {
					running = false;
					break;
				}
				values[counter] = num; //collect number input
				
				//add to array size
				int[] bigger = new int[(values.length) + 1];
				for (int i=0; i<values.length; i++) { bigger[i] = values[i]; }
				values = bigger; bigger = null;
				
				counter++;
			} catch (Exception e) { //check for invalid entries
				System.out.println("Invalid Entry");
			}
		}
		
		System.out.println("Where would you like to write the data?"); //prompt data location	
		boolean running2 = true;
		while(running2) {
			try {
				String dir = input.nextLine();
				FileWriter f = new FileWriter("src/labs/lab3/" + dir); //initialize filewriter object
				System.out.println("Writing to file");
				for (int i=0; i<(values.length - 1); i++) { //write each array element to file
					f.write(values[i] + "\n");
				}
				f.flush();
				f.close();
				running2 = false;
			} catch (IOException e) { //look for errors in filewriting
				System.out.println("Invalid directory");
		}
		}
		System.out.println("Goodbye");
		
	}
}
