package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) throws IOException {
		// Grade averaging program, Charles Paguia, CS 201 Boliske, 1.25.2022
		// Goal: Calculate grade averages based off grades.csv file
		
		File file = new File("src/labs/lab3/grades.csv"); // initialize file
		Scanner input = new Scanner(file); //initialize scanner
		
		double total = 0; //initialize total
		int counter = 0; //initialize counter variables to count amount of entries
		while(input.hasNextLine()) { //read in data as long as there is more to read
			counter++; //counter how many entries to use in average calculation
			String line = input.nextLine(); //read lines
			String sum = line.substring(line.indexOf(',')+1); //take number from line after the comma
			double number = Integer.parseInt(sum); //parse string sum to int
			total = number + total; //add total to itself each time it runs
		
		input.close(); //close input
		}
		double avg = total / counter; //calculate average
		System.out.println("The class average is " + avg); //print class average
		}
	}
