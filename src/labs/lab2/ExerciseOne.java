package labs.lab2;

import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) {
		// Square program, Charles Paguia, CS 201 Boliske, 1.25.2022
		// Goal: Print square with dimensions from user input
		
		//Plan: create while loop using a counter for the rows, and a simple multiplication for the columns
		
		Scanner input = new Scanner(System.in); //Create scanner object
		System.out.print("What would you like the side dimensions of the square to be? "); //Prompt user
		double dside = input.nextDouble(); //make input able to handle doubles without crashing
		int side = (int) (dside / 1); // if user input is a double; truncate it and use that instead
		
		input.close();
		
		if(side >= 100) { //overloading fail-safe
			System.out.println("That's too big"); 
		} else {
			
			int counter = 0; //create counter for creating rows
			int counter2 = 0; //create counter for creating columns
			
			//makes it so using the counters, loop will print as many '*' as the input, and once it is done with the row
			//the column counter(counter2) is reset back to 0, so the loop will start over and create another row
			//this continues until the row counter(counter) is equal to the input, then it will stop printing
			while(counter != side) { 
				while (counter2 != side) {
					System.out.print(" *");
					counter2 = counter2 + 1;
				}
				counter2 = 0;
				System.out.println();
				counter = counter + 1;
			}
		}
	}
}
