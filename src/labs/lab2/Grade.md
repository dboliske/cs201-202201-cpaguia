# Lab 2

## Total

19/20

## Break Down

* Exercise 1    6/6
* Exercise 2    6/6
* Exercise 3    5/6
* Documentation 2/2

## Comments
- Ex3 doesn't handle decimals (floating points)
- Great job on Ex2 grade constraints