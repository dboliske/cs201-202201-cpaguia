package labs.lab2;

import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) {
		// Grade averaging program, Charles Paguia, CS 201 Boliske, 1.25.2022
		// Goal: Calculate an unspecified amount of grade averages based on user input
		
		Scanner input = new Scanner(System.in); //Create scanner object
		int counter = 0; //create counter for averaging calculation (average = sum/counter)
		double sum = 0; //create sum var for averaging calculation
		double grade = 0; //initialize grade var for collecting user input
		
		while(grade != -1 && grade < 100) { // run as long as input is not -1
			System.out.print("Enter a grade (type -1 to cancel): "); // prompt user for grades
			grade = input.nextDouble(); // collect input as variable grade
			if(grade != -1 && grade < 100) { // makes it so if input is -1, it will not be included in the calculation
				sum = grade + sum; // add input(grade) to sum
				counter = counter + 1; // amount of entries (counter) is increased with each entry
			} else if (grade > 100) { // if grade is greater than 100 prints the number is too large
				System.out.println("This number is too large");
			} else if (grade == -1) { // if input is -1 then program ends
				System.out.println("The program is ending");
			}	
		}
		input.close(); //close input then calculate
		double average = sum / counter; //calculate average
		System.out.println("Grade average: " + average); //print out average
	}
}
