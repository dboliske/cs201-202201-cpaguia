package labs.lab2;

import java.util.Scanner;

public class ExerciseThree {

	public static void main(String[] args) {
		// Menu program, Charles Paguia, CS 201 Boliske, 1.25.2022
		// Goal: Display menu options and act according to user selection until user exits program
		
		Scanner input = new Scanner(System.in); //create scanner object
		boolean running = true;
		
		System.out.println("Hello! Please choose what you would like to do."); //introduce program
		
		while(running) {
			//display menu
			System.out.println(" 1. Say Hello");
			System.out.println(" 2. Addition");
			System.out.println(" 3. Multiplication");
			System.out.println(" 4. Exit");
			System.out.println("Enter choice: "); //prompt user for input
			int selection = input.nextInt(); // collect input and put it at selection
			
			switch (selection) {
				case 1:
					System.out.println("Hello user."); // if user selects 1 print hello
					break;
				case 2:
					System.out.print("Program: Summation\nEnter first number: "); //Prompt user for first number
					int num1 = input.nextInt();
					System.out.print("Enter second number: "); //Prompt user for second number
					int num2 = input.nextInt();
					int sum = num1 + num2; // find the sum of the numbers
					System.out.println("The sum of " + num1 + " and " + num2 + " is " + sum + "."); //print sum
					break;
				case 3:
					System.out.print("Program: Multiplication\nEnter first number: "); //Prompt user for first number
					int mult1 = input.nextInt();
					System.out.print("Enter second number: "); //Prompt user for second number
					int mult2 = input.nextInt();
					int product = mult1 * mult2; // find product of the numbers
					System.out.println("The product of " + mult1 + " and " + mult2 + " is " + product + "."); //print product
					break;
				case 4:
					running = false; //change flag to false so loop no longer executes
					break;
				default:
			}
		}	
	System.out.println("Goodbye.");
	input.close();
	}
}
