package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.jar.Attributes.Name;

public class Deli {
	
	private String name;
	
	public Deli() {
		this.name = "Noah";
	}
	
	public Deli(String name) {
		this.name = name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
