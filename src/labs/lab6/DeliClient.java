package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.jar.Attributes.Name;

public class DeliClient {
	
	public static ArrayList<Deli> menu(Scanner input, ArrayList<Deli> customers) {
		boolean done = false;
		do { //print menu while not done
			System.out.println("Menu\n1. Add customer to queue\n2. Help Customer\n3. Exit.");
			try { //try to catch input errors
				int choice = Integer.parseInt(input.nextLine());
				switch (choice) {
					case 1: //add customer
						addCustomer(input, customers);
						break;
					case 2: //help customer
						helpCustomer(customers);
						break;
					case 3: //exit
						done = true;
						break;	
				}
			} catch (Exception e) { //catch error
				System.out.println(e.getMessage());
			}
		} while (!done);
		return customers;
	}
	
	public static ArrayList<Deli> addCustomer(Scanner input, ArrayList<Deli> customers) {
		Deli customer;
		System.out.println("What is the customers name?: ");
		String name = input.nextLine();
		customer = new Deli(name);
		customers.add(customer);
		System.out.println("Customer " + name + " is number " + (customers.indexOf(customer) + 1) + " in the queue");
		return customers;
	}
	
	public static ArrayList<Deli> helpCustomer(ArrayList<Deli> customers) {
		System.out.println("Helping " + customers.get(0));
		System.out.println(customers.get(0) + " has been helped.");
		customers.remove(0);
		return customers;
	}
	
	public static void main(String[] args) {
		//create scanner in main
		Scanner input = new Scanner(System.in);
		//create ArrayList
		ArrayList<Deli> customers = new ArrayList<Deli>();
		//pass ArrayList and scanner into menu.
		customers = menu(input, customers);
		
		//close scanner and close program
		input.close();
		System.out.println("Goodbye.");
	}
	
}
