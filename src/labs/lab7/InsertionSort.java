package labs.lab7;

public class InsertionSort {

	public static String[] Sort(String[] array) {
		for (int j=1; j<array.length; j++) {
			int i = j;
			while (i > 0 && array[i].compareTo(array[i-1]) < 0) {
				swap(array, i, i-1);
				i--;
			}
		}
		
		return array;
	}
	
	
	public static String[] swap(String[] array, int index1, int index2) {
		String temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;		
		return array;
	}
	
	public static void main(String[] args) {
		String[] array = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		array = Sort(array);
		
		for (String n : array) {
			System.out.print(n + " ");
		}
	}
}
