package labs.lab7;

import java.util.Scanner;

public class BinarySearch {

	public static int BinarySearch(String[] array, String value) {
		//initialize variables
		int start = 0;
		int end = array.length;
		int pos = -1;
		boolean found = false;
		//while not found and start isnt at the end (while not 1 item array)
		while (!found && start != end) {
			//middle of array is in the middle
			int middle = (start + end) / 2;
			//if middle value is value you're looking for, it is found
			if (array[middle].equalsIgnoreCase(value)) {
				found = true;
				pos = middle; //now searching from the middle
			} else if (array[middle].compareToIgnoreCase(value) < 0) { //else if 
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		return pos;
	}
	
	public static void main(String[] args) {
		String[] array = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search for: ");
		String value = input.nextLine();
		int index = BinarySearch(array, value);
		if (index == -1) {
			System.out.println("Can't find " + value);
		} else {
			System.out.println("Found " + value + " at index " + index);
		}
		input.close();
	}

}
