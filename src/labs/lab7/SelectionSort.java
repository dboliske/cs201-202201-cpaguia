package labs.lab7;

public class SelectionSort {

	public static double[] Sort(double[] array) {
		for (int i=0; i<array.length - 1; i++) {
			int min = i;
			for (int j=i+1; j < array.length; j++) {
				if (array[j] < array[min]) {
					min = j;
				}
			}
			if (min != i) {
				swap(array, i, min);
			}
		}
		return array;
	}
	
	public static double[] swap(double[] array, int index1, int index2) {
		double temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;		
		return array;
	}
	
	public static void main(String[] args) {
		double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		array = Sort(array);
		
		for (double n : array) {
			System.out.print(n + " ");
		}
	}

}
