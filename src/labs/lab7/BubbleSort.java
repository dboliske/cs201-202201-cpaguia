package labs.lab7;

public class BubbleSort {

	public static int[] Sort(int[] array) {
		boolean sorted = false;
		do {
			sorted = true;
			for (int i=0; i<array.length - 1; i++) {
				if (array[i+1] < array[i]) {
					swap(array, i+1, i);
					sorted = false;
				}
			}
		} while (!sorted);
		
		return array;
	}
	
	public static int[] swap(int[] array, int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;		
		return array;
	}
	

	public static void main(String[] args) {
		int[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		array = Sort(array);
		
		for (int i : array) {
			System.out.print(i + " ");
		}
	}
	
}
	
	