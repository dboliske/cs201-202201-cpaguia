package labs.lab0;

public class Square {

	public static void main(String[] args) {
		// Name and Birthdate Program, Charles Paguia, CS 201 Boliske, 1.18.2022
		// Goal: Output "My name is Charles Paguia and my birthdate is Dec. 1, 2001" to console
		
		// Print top of square "__________"
		// Print middle of square "|         |"
		// Repeat middle of square multiple times
		// Print bottom of square "|__________|"
		
		//Code psuedocode
		System.out.println("._________.");
		System.out.println("|         |");
		System.out.println("|         |");
		System.out.println("|         |");
		System.out.println("|_________|");
		
		
		// I got the desired results

	}

}
