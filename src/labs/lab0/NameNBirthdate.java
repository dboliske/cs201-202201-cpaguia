package labs.lab0;

public class NameNBirthdate {

	public static void main(String[] args) {
		// Name and Birthdate Program, Charles Paguia, CS 201 Boliske, 1.18.2022
		// Goal: Output "My name is Charles Paguia and my birthdate is Dec. 1, 2001" to console
		
		// Set name and birthdate
		String name = "Charles Paguia";
		String birthdate = "Dec. 1, 2001";
		
		// Print to console using variables
		System.out.println("My name is " + name + " and my birthdate is " + birthdate);
		

	}

}
