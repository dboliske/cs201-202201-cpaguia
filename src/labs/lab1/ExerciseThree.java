package labs.lab1;

import java.util.Scanner;

public class ExerciseThree {

	public static void main(String[] args) {
		// Name initials program, Charles Paguia, CS 201 Boliske, 1.21.2022
		// Goal: Prompt user for name, echo their initials
		
		Scanner input = new Scanner(System.in); // Create scanner object
		System.out.print("What is yourv first name? "); // Prompt user for name
		String name = input.nextLine(); // Set scanner input to name
		
		char initial = name.charAt(0); // make first initial variable. set to first letter of name
		System.out.println("First initial: " + initial); // print out first initial
		
		input.close(); // Close input
		
	}

}
