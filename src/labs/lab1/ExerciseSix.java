package labs.lab1;

import java.util.Scanner;

public class ExerciseSix {

	public static void main(String[] args) {
		// Inch to Centimeters program, Charles Paguia, CS 201 Boliske, 1.24.2022
		// Goal: Convert inches to centimeters
		
		Scanner input = new Scanner(System.in); // Create scanner object
		System.out.print("Inches: "); // prompt user
		Double inches = input.nextDouble(); //Take input. set as inches
		
		Double centimeters = inches * 2.54; //convert inches to centimeters
		System.out.println("Centimeters: " + centimeters); //output conversion
		
		input.close(); //close scanner
		
		//Test Table
		//Input				Expected			Actual
		//1						2.54			2.54
		//5.4					3.716			13.716666666
		//12					30.48			30.48
		//19.685				50				49.9999
	}

}
