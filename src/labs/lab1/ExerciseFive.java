package labs.lab1;

import java.util.Scanner;

public class ExerciseFive {

	public static void main(String[] args) {
		// Wooden box program, Charles Paguia, CS 201 Boliske, 1.24.2022
		// Goal: Prompt user for length, width, and depth in inches of box. calculate surface area (wood needed)
		
		Scanner input = new Scanner(System.in); //create scanner object
		System.out.print("Length of box (inches): "); //prompt user for length
		double length = input.nextDouble(); //store length
		System.out.print("Width of box (inches): "); //prompt user for length
		double width = input.nextDouble(); //store length
		System.out.print("Depth of box (inches): "); //prompt user for length
		double depth = input.nextDouble(); //store length
		
		input.close(); // close input
		
		//calculate surface area of box
		// SA = 2lw + 2lh + 2wh
		double srfAreaIn = (2 * (length * width)) + (2 * (length * depth)) + (2 * ( width * depth));
		//convert SA to square feet
		double srfAreaFt = (srfAreaIn / 144);
		//print how much square feet of wood you need
		System.out.println("You need " + srfAreaIn + " square inches of wood or " + srfAreaFt + " square feet of wood.");
		
		
		//Test table
		//Inputs				Expected Output			Actual output
		//4,7,9					1.7638					1.7638
		//5,10,2				1.1111					1.1111
		//6,2,8					1.0555					1.0555
		

	}

}
