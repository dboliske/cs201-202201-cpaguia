package labs.lab1;

import java.util.Scanner;

public class ExerciseOne {

		public static void main(String[] args) {
		// Name input program, Charles Paguia, CS 201 Boliske, 1.21.2022
		// Goal: Ask for user input of a name. Echo name back to user.
				
		Scanner input = new Scanner(System.in); // Declare scanner object
		System.out.print("What is your name? ");	// Prompt user for their name
		String name = input.nextLine(); // Set user input to name
			
		input.close(); // Close scanner
				
		System.out.println(name);	// Echo user name


	}

}
