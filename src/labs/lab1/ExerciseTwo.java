package labs.lab1;

public class ExerciseTwo {

	public static void main(String[] args) {
		// Arithmetic program, Charles Paguia, CS 201 Boliske, 1.21.2022
		// Goal: Calculate various values
		
		int myAge = 20; // set my age
		int dadAge = 60; // set my dads age
		
		System.out.println("My dad's age minus my age is " + (dadAge - myAge)); // print dadAge - myAge
		
		int heightIn = 70; // set my height in inches
		double heightCm = heightIn * 2.54; // convert height in inches to height in centimeters by multiplying by 2.54
		
		System.out.println("Height in inches is " + heightIn + ". This height in centimeters is " + heightCm); //print heightIn, and the conversion heightCm
		
		int heightFt = heightIn / 12; // get how tall in feet (not including inches)
		int heightFtIn = heightIn % 12; // get how tall in inches (not including feet)
		
		System.out.println("My height is " + heightFt + "'" + heightFtIn + '"'); // print height in feet and inches
				

	}

}
