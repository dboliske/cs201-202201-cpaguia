package labs.lab1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ExerciseFour {

	public static void main(String[] args) {
		// Temperature program, Charles Paguia, CS 201 Boliske, 1.24.2022
		// Goal: Prompt user for temp in fahrenheit, convert to celsius. Prompt temp in C, convert to F.
		
		Scanner input = new Scanner(System.in); // create scanner object
		System.out.print("Please input a temperature in Fahrenheit: "); //prompt user for input
		double inputTempF = input.nextDouble(); //collect user input of fahrenheit temperature
		double celsius = (inputTempF - 32) * (5.0/9.0); //convert inputTempF to celsius
		
		//wanted to make it so if the conversion had a .0 it was not shown --> imported DecimalFormat
		DecimalFormat df = new DecimalFormat();
		
		//output converted temp (\u00b0 is the degrees symbol)
		System.out.println(inputTempF + "\u00B0 F in Celsius is: " + df.format(celsius) + "\u00B0 C");
		
		
//---------------------------------------------------------------------------------------------------------------
		
		
		//plan: repeat code above but adjust arithmetic so C goes to F
		
		System.out.print("Please input a temperature in Celsius: "); //prompt user for input
		double inputTempC = input.nextDouble(); //collect user input. convert to celsius temp 
		double fahrenheit = (inputTempC*(1.8)) + 32; //convert inputTempF to celsius
				
		//output converted temp (\u00b0 is the degrees symbol)
		System.out.println(inputTempC + "\u00B0 C in Fahrenheit is: " + df.format(fahrenheit) + "\u00B0 F");	
		
		input.close(); //close scanner
		
//---------------------------------------------------------------------------------------------------------------
		
		//TEST TABLE            WORKS BOTH WAYS
		//32F is 0C					yes
		//72F is 22.22C				yes
		//72.05F is 22.25C			yes
		//86F is 30C				yes
		//212F is 100C				yes
		
		
	}

}
