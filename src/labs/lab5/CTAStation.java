package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class CTAStation extends GeoLocation {
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		super();
		name = "Chicago";
		setName(name);
		location = "elevated";
		setLocation(location);
		wheelchair = true;
		setWheelchair(wheelchair);
		open = true;
		setOpen(open);
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = "Chicago";
		setName(name);
		this.location = "elevated";
		setLocation(location);
		this.wheelchair = true;
		setWheelchair(wheelchair);
		this.open = true;
		setOpen(open);
	}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public String toString() {
		return super.toString() + "Station " + name + " it is " + location + 
				". Has wheelchair: " + wheelchair + ". Open: " + open;
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else {
			return true;
		}
	}

}



