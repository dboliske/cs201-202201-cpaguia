package labs.lab5;

//GeoLocation program, Charles Paguia, CS 201 Boliske, 2.21.2022
//Goal: Create GeoLocation methods

public class GeoLocation {

	private double lat;
	private double lng;
	private double distance;
	
	public GeoLocation() { //initialize geolocation class
		lng = 0.0;
		lat = 0.0;
	}
	
	public GeoLocation(double lat, double lng) { //put parameters into geolocation class
		this.lat = 0.0;
		setLat(lat);
		this.lng = 0.0;
		setLng(lng);
	}

	public void setLat(double lat) { //setter methods
		if (validLat(lat)) { //check whether lat is valid before setting it
			this.lat = lat;
		}
	}
	
	public void setLng(double lng) {
		if (validLng(lng)) {
			this.lng = lng;
		}
	}
	
	public double getLat() { //getter methods
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public String toString() { //tostring method
		return "Coordinates: " + lat + ", " + lng + ". ";
	}
	
	public boolean validLat(double lat) { //make sure latitude is valid (min 90s and max 90n)
		if (lat > -90 && lat < 90) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validLng(double lng) { //make sure longitude is valid (min 180e and max 180w)
		if (lng > -180 && lng < 180) {
			return true;
		} else {
			return false;
		}
	}
	
		public boolean validLat() { //make sure latitude is valid (min 90s and max 90n)
		if (lat > -90 && lat < 90) {
			return true;
		} else {
			return false;
		}
	}
		
	public boolean validLng() { //make sure longitude is valid (min 180e and max 180w)
		if (lng > -180 && lng < 180) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(GeoLocation g) { //equals method
		if (this.lat == g.getLat() && this.lng == g.getLng()) {
			return true;
		} else {
			return false;
		}
	}
	
	public double calcDistance(GeoLocation g) {
		distance = 1;
		distance = Math.sqrt(Math.pow(getLat() - g.getLat(), 2) + Math.pow(getLng() - g.getLng(), 2));
		return distance;
	}
	
	public double calcDistance(double lat, double lng) {
		distance = 1;
		distance = Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
		return distance;
	}
	
	
	
}
