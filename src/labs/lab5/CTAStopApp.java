package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class CTAStopApp {
	
	public static CTAStation[] readFile(String filename) {
		CTAStation[] stations = new CTAStation[34];
		//Local scanner is created here to read file
		
		//create try block to handle fnf exception
		try {
			File file = new File(filename);
			Scanner input = new Scanner(file);
			
			int counter = 0;
			//end-of-file controlled loop
			while (input.hasNextLine()) {
				try {
					//read line of CTAStops.csv
				String line = input.nextLine();
				String[] values = line.split(",", 6); //split line at commas
				CTAStation station = new CTAStation(
						values[0], 							//name
						Double.parseDouble(values[1]), 		//latitude
						Double.parseDouble(values[2]), 		//longitude
						values[3], 							//location
						Boolean.parseBoolean(values[4]), 	//wheelchair
						Boolean.parseBoolean(values[5])); 	//open
				
				//add instance of station to CTAStation[] stations
				stations[counter] = station;
				counter++;
				} catch (Exception e) {
					System.out.println("Error reading in data");
				}
				
			}
			
			
			input.close();
		} catch (FileNotFoundException fnf) {
			//file not found
		} catch (Exception e) {
			System.out.println("Error reading file");
		}
		
		return stations;
	}
	
	public static CTAStation[] menu(Scanner input, CTAStation[] stations) {
		//scanner is passed into here from main. input is closed later in main
		boolean running = true; //create loop flag
		
		do {
			//print menu
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations w/ or w/o wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.println("Choice: ");
			String choice = input.nextLine();
			
			//read menu choice
			switch (choice) {
				case "1": //display station names
					displayStationNames(stations);
					break;
				case "2": //display stations w wheelchair access
					displayByWheelchair(input, stations);
					break;
				case "3": //display nearest station
					displayNearest(input, stations);
					break;
				case "4": //exit
					running = false;
					break;
				default:
					System.out.println("Invalid choice");
			} 
		} while (running);
		
		
		return stations;
	}
	
	public static void displayStationNames(CTAStation[] stations) {
		for (int i=0; i<stations.length; i++) { //loop through stations. list names.
			System.out.println(stations[i]);
		}
	}
	
	public static CTAStation[] displayByWheelchair(Scanner input, CTAStation[] stations) {
		//ask y or no to wheel chair
		System.out.println("Check for station with wheelchairs? (y or n)");
		String choice = input.nextLine();
		switch (choice) {
			case "y":
				//loop through station in array. print out the stations name only if wheelchair is true
				//print getName() if hasWheelchair() = true
				int wcAmount = 0;
				for (int i = 0; i < stations.length; i++) {
					if (stations[i].hasWheelchair()) {
						System.out.println("Station " + stations[i].getName());
					} else {
						wcAmount++;
					}					
				}
				if (wcAmount == 0) {
					System.out.println("No stations found with wheelchairs");
				}
				
				break;
			case "n":
				//print getName() if hasWheelchair() = false
				//if block: if none, print none
				int wcAmount2 = 0;
				for (int i = 0; i < stations.length; i++) {
					if (!stations[i].hasWheelchair()) {
						System.out.println("Station " + stations[i].getName());
					} else {
						wcAmount2++;
					}					
				}
				if (wcAmount2 == 0) {
					System.out.println("No stations found without wheelchairs");
				}
				break;
			default:
				System.out.println("Invalid choice");
		}
		
		return stations;
	}
	
	public static CTAStation[] displayNearest(Scanner input, CTAStation[] stations) {
		//create new instance g, ask for lat and lng 
		try {
			System.out.println("What are you coordinates?");
			System.out.println("Latitude: ");
			double lat = input.nextDouble();
			System.out.println("Longitude: ");
			double lng = input.nextDouble();
			
			double nearest = 10000000; //nearest starts off very big (so that first dist will always be nearest)
			double distance; //initialize distance
			String nearestStation = null; //initialize nearestStation
			for (int i = 0; i < stations.length; i++) { //loop through stations array
				distance = stations[i].calcDistance(lat, lng); //calc distance between stations and location
				if (distance < nearest) { //sort through to find the nearest. if it is nearest, set neareststation
					nearest = distance;
					nearestStation = stations[i].getName();
				} else {
					nearest = nearest;
				}
				
			}
			System.out.println("The nearest station is " + nearestStation);

		} catch (Exception e) {
			System.out.println("Invalid input. Try again.");
		}
		return stations;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //create scanner in main
		//load and read file CTAStops.csv
		String filename = "src/labs/lab5/CTAStops.csv";
		CTAStation[] stations = readFile(filename);
		//menu
		stations = menu(input, stations);
		//close program
		input.close();
		System.out.println("Goodbye");
		
	}
	
	
}
