package project;

public class AgeRestricted extends Item {

	
	//CHARLES PAGUIA
	//4/27/2020
	//AGERESTRICTED: INHERITS FROM ITEM CLASS. HAS A MINIMUM AGE LIMIT.
	
	private int ageLimit;
	
	//DEFAULT CONSTRUCTOR.
	public AgeRestricted() {
		super();
		this.ageLimit = 21;
	}
	
	//CONSTRUCTOR. INITIALIZES AND SETS AGELIMIT.
	public AgeRestricted(String name, double price, int ageLimit) {
		super(name, price);
		this.ageLimit = 21;
		setAgeLimit(ageLimit);
	}

	//AGE LIMIT GETTER.
	public int getAgeLimit() {
		return ageLimit;
	}
	
	//AGE LIMIT SETTER.
	public void setAgeLimit(int ageLimit) {
		if (ageLimit > 0) {
			this.ageLimit = ageLimit;
		}
	}
	
	//AGE RESTRICTED TOSTRING. OVERRIDES ITEM TOSTRING.
	@Override
	public String toString() {
		return super.toString() + ". Restricted to " + ageLimit + " years old";
	}
	
	//AGE RESTRICTED EQUALS. OVERRIDES ITEM EQUALS.
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) { //check name and price
			return false;
		} else if (!(obj instanceof AgeRestricted)) { //check if produce
			return false;
		}
		//type cast object to produce
		AgeRestricted a = (AgeRestricted)obj;
		if (this.ageLimit != a.getAgeLimit()) {
			return false;
		}
		//if everything matches, item is equal to object obj
		return true;
	}
	
	//AGE RESTRICTED TOCSV. OVERRIDES ITEM TOCSV. USED IN WRITING TO FILE.
	@Override
	public String toCSV() {
		return super.toCSV() + "," + ageLimit;
	}
	
}
