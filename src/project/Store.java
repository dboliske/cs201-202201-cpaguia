package project;

import java.util.Scanner;

public class Store {
	
	//CHARLES PAGUIA
	//4/27/2020
	//STORE: SUPER CLASS TO STOREAPP. ALLOWS STOREAPP TO CREATE ITEMS.
	//CONTAINS METHODS FOR CERTAIN PROMPTS THAT DO NOT DEAL WITH ARRAYLISTS. HELPS ORGANIZE PROJECT.
	
	//CREATE ITEM. PROMPTS FOR NAME AND PRICE.
	protected static Item createItem(Scanner input) {
		//prompt for item name
		String name = "item";
		System.out.print("Item name: ");
		try { //try getting item name
			name = input.nextLine();
		} catch (Exception e) {
			System.out.println("Error reading name. Setting name to 'item'.");
		}
		//set price to 0. continuously prompt user if it's an invalid price
		double price = 0.00;
		do {
			try {
				System.out.print("Item price: ");
				price = Double.parseDouble(input.nextLine());
			} catch (NumberFormatException nfe) {
				System.out.println("Invalid entry.");
			} catch (Exception e) {
				System.out.println("Error.");
			}
		} while (price < 0);
		//create item with user inputted name and price
		Item item = new Item(name, price);
		return item;
	}
	
	//PROMPTS FOR EXPIRATION MONTH FOR PRODUCE.
	public static int expMonthPrompt(Scanner input) {
		int expMonth = 5;
		System.out.print("Expiration month (number): ");
		try {
			expMonth = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid entry. Setting month to 05.");
		}
		return expMonth;
	}
	
	//PROMPTS FOR EXPIRATIN DAY FOR PRODUCE.
	public static int expDayPrompt(Scanner input) {
		int expDay = 25;
		System.out.print("Expiration day (number): ");
		try {
			expDay = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid entry. Setting day to 25.");
		}
		return expDay;
	}

	//PROMPTS FOR EXPIRATION YEAR FOR PRODUCE.
	public static int expYearPrompt(Scanner input) {
		int expYear = 2020;
		System.out.print("Expiration year (number): ");
		try {
			expYear = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid entry. Setting year to 2020.");
		}
		return expYear;
	}
	
	//CLONES ITEM UTILIZING CREATEITEM AND OR CREATESHELVED / CREATEPRODUCE / CREATEAGERESTRICTED.
	public static Item cloneItem(Item item) {
		if (item instanceof Produce) {
			Produce clonedItem = new Produce();
			clonedItem.setName(item.getName());
			clonedItem.setPrice(item.getPrice());
			//parse items expiration date and format it
			int month = 5;
			int day = 25;
			int year = 2020;
			String[] values = ((Produce) item).getExpDate().split("/", 3);
			month = Integer.parseInt(values[0]);
			day = Integer.parseInt(values[1]);
			year = Integer.parseInt(values[2]);
			//pass month day and year into setter
			try {
				clonedItem.setExpDate(month, day, year);
			} catch (Exception e) {
				System.out.println("Error.");
			}
			return clonedItem;
		} else if (item instanceof AgeRestricted) {
			AgeRestricted clonedItem = new AgeRestricted();
			clonedItem.setName(item.getName());
			clonedItem.setPrice(item.getPrice());
			try {
				clonedItem.setAgeLimit(((AgeRestricted)item).getAgeLimit());
			} catch (Exception e) {
				System.out.println("Error.");
			}
			return clonedItem;
		} else {
			Shelved clonedItem = new Shelved();
			clonedItem.setName(item.getName());
			clonedItem.setPrice(item.getPrice());
			return clonedItem;
		}
	}
	
	//CREATE PRODUCE. UTILIZES CREATE ITEM AND EXPMONTH EXPDAY AND EXPYEAR PROMPT TO CREATE EXPIRATION DATE.
	protected static Produce createProduce(Scanner input) {
		//ask for items name and price
		Item item = createItem(input);
		//prompt for expiration date
		int expMonth = expMonthPrompt(input);
		int expDay = expDayPrompt(input);
		int expYear = expYearPrompt(input);
		//create produce item
		Produce produce = new Produce();
		produce.setName(item.getName());
		produce.setPrice(item.getPrice());
		produce.setExpDate(expMonth, expDay, expYear);
		return produce;
	}

	//PROMPTS FOR AGE LIMIT FOR AGE RESTRICTED.
	protected static int ageLimitPrompt(Scanner input) {
		int ageLimit = 0;
		do {
			try {
				System.out.print("Age limit: ");
				ageLimit = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException nfe) {
				System.out.println("Invalid entry.");
			}
		} while (ageLimit <= 0);
		return ageLimit;
	}
	
	//CREATE AGE RESTRICTED. UTILIZES CREATE ITEM AND AGELIMIT PROMPT TO CREATE AGE LIMIT.
	protected static AgeRestricted createAgeRestricted(Scanner input) {
		//create blank age-restricted item to add
		AgeRestricted agerestricted = new AgeRestricted();
		//ask for items name and price
		Item item = createItem(input);
		//set age-restricted name and price using createItem item
		agerestricted.setName(item.getName());
		agerestricted.setPrice(item.getPrice());
		//prompt for age restriction
		int ageLimit = ageLimitPrompt(input);
		agerestricted.setAgeLimit(ageLimit);
		return agerestricted;
	}
	
	//CREATE SHELVED.
	protected static Shelved createShelved(Scanner input) {
		//create blank shelved to add
		Shelved shelved = new Shelved();
		//ask for items name and price
		Item item = createItem(input);
		//set name and price using createItem item
		shelved.setName(item.getName());
		shelved.setPrice(item.getPrice());
		return shelved;
	}	
	
}
