package project;

public class Produce extends Item {
	
	//CHARLES PAGUIA
	//4/27/2020
	//PRODUCE: INHERITS FROM ITEM CLASS. PRODUCE EXPIRES AND REQUIRES AN EXPIRATION DATE.
	
	private String expDate;
	
	//DEFAULT CONSTRUCTOR.
	public Produce() {
		super();
		this.expDate = "05/25/2020";
	}
	
	//CONSTRUCTOR. INITIALIZES AND SETS EXPIRATION DATE. PARSES STRING EXPDATE INTO NUMBERS SO THAT
	//EXPIRATION DATE CAN BE VALIDATED EASIER.
	public Produce(String name, double price, String expDate) {
		super(name, price);
		//set default expiration date
		this.expDate = "05/25/2020";
		//parse string into numbers, then set as expDate
		int month = 5;
		int day = 25;
		int year = 2020;
		String[] values = expDate.split("/", 3);
		month = Integer.parseInt(values[0]);
		day = Integer.parseInt(values[1]);
		year = Integer.parseInt(values[2]);
		//pass month day and year into setter
		setExpDate(month, day, year);
	}

	//EXPIRATION DATE GETTER.
	public String getExpDate() {
		return expDate;
	}
	
	//EXPIRATION DATE SETTER. FIRSTS CREATES AN EXPDATE, VALIDATES IT, THEN SETS IT.
	public void setExpDate(int month, int day, int year) {
		String expDate = createExpDate(month, day, year);
		//if date input is invalid, createExpDate outputs null expDate.
		this.expDate = expDate;
	}
	
	//CHECKS IF VALID DATE. FORMATS IT. RETURNS STRING EXPDATE TO BE SET.
	private String createExpDate(int month, int day, int year) {
		boolean validMonth = false;
		boolean validYear = false;
		boolean validDay = false;
		String strExpMonth = "05";
		String strExpDay = "25";
		
		//validate month
		if (month > 0 && month <= 12) { //restrict month 1 to 12
			validMonth = true;
			strExpMonth = formatMonth(month);
		}
		
		//validate year
		//keep to 4 digits (more or less doesnt make sense for expiration date)
		if (year > 999 && year <= 9999) {
			validYear = true;
		}
		
		//validate day
		if (month == 2) { // if Feb
			if (year % 4 == 0) { //if leap year
				if (day > 0 && day <= 29) { //Feb has 29 days
					validDay = true;
					strExpDay = formatDay(day);
				}
			} else { //if not leap year
				if (day > 0 && day <= 28) { //feb has 28 days
					validDay = true;
					strExpDay = formatDay(day);
				}
			}
			//if Apr Jun Sep or Nov
		} else if (month == 4 || month == 6 || month == 9 || month == 11) {
			if (day > 0 && day <= 30) { //month has 30 days
				validDay = true;
				strExpDay = formatDay(day);
			}
		} else { //if not feb apr jun sep or nov
			if (day > 0 && day <= 31) { // month has 31 days
				validDay = true;
				strExpDay = formatDay(day);
			}
		}
		
		//IF VALID: FORMAT DATE. IF INVALID: DATE WILL BE NULL
		String expDate = (validMonth && validYear && validDay) ? (strExpMonth + "/" + strExpDay + "/" + Integer.toString(year)) : null;
		return expDate;
	}
	
	//ADDS LEADING 0 TO MONTH IF SINGLE DIGIT. USED IN CREATEEXPDATE().
	private String formatMonth(int month) {
		String strExpMonth;
		if (month > 0 && month <= 9) { //if single digit
			strExpMonth = "0" + Integer.toString(month); //add leading 0
		}
		strExpMonth = Integer.toString(month);
		return strExpMonth;
	}

	//ADDS LEADING 0 TO DAY IF SINGLE DIGIT. USED IN CREATEEXPDATE().
	private String formatDay(int day) {
		String strExpDay;
		if (day > 0 && day <= 9) { //if single digit
			strExpDay = "0" + Integer.toString(day); //add leading 0
			return strExpDay;
		}
		strExpDay = Integer.toString(day);
		return strExpDay;
	}
	
	//PRODUCE TOSTRING. OVERRIDES ITEM TOSTRING.
	@Override
	public String toString() {
		return super.toString() + ". Expires on " + expDate;
	}
	
	//PRODUCE EQUALS. OVERRIDES ITEM EQUALS.
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) { //check name and price
			return false;
		} else if (!(obj instanceof Produce)) { //check if produce
			return false;
		}
		//type cast object to produce
		Produce p = (Produce)obj;
		if (!this.expDate.equals(p.getExpDate())) {
			return false;
		}
		//if everything matches, item is equal to object obj
		return true;
	}
	
	//PRODUCE TOCSV. OVERRIDES ITEM TOCSV. USED IN WRITING TO FILE.
	@Override
	public String toCSV() {
		return super.toCSV() + "," + expDate;
	}
	
}