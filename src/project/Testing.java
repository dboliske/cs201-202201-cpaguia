package project;

import java.util.ArrayList;

public class Testing {

	public static void main(String[] args) {
		//CLIENT TO CHECK THAT ITEMS WORK
		
		//MAKE SURE EXPIRATION DATES WITH PRODUCE WORK 
		//check default constructors
		Produce p = new Produce();
		System.out.println("produce 1");
		System.out.println(p.getName());
		System.out.println(p.getPrice());
		System.out.println(p.getExpDate());
		System.out.println(p.toCSV());
		System.out.println();
		//WORKS
		
		//check valid day
		System.out.println("produce 2");
		Produce p2 = new Produce();
		p2.setName("banana");
		p2.setPrice(1.03);
		p2.setExpDate(3, 28, 2026);
		System.out.println(p2.getName());
		System.out.println(p2.getPrice());
		System.out.println(p2.getExpDate());
		System.out.println(p2.toString());
		System.out.println(p2.toCSV());
		System.out.println();
		//WORKS
		
		//check leap day
		System.out.println("produce 3");
		Produce p3 = new Produce();
		p3.setName("banana");
		p3.setPrice(1.03);
		p3.setExpDate(2, 29, 2020);
		System.out.println(p3.getName());
		System.out.println(p3.getPrice());
		System.out.println(p3.getExpDate());
		System.out.println(p3.toCSV());
		System.out.println();
		//WORKS
		
		//check invalid leap year
		System.out.println("produce 4");
		Produce p4 = new Produce();
		p4.setName("banana");
		p4.setPrice(1.03);
		p4.setExpDate(2, 2021, 29);
		System.out.println(p4.getName());
		System.out.println(p4.getPrice());
		System.out.println(p4.getExpDate());
		System.out.println(p4.toCSV());
		System.out.println();
		//WORKS (invalid day is not set)
		
		//check invalid day in month
		System.out.println("produce 5");
		Produce p5 = new Produce();
		p5.setName("banana");
		p5.setPrice(1.03);
		System.out.println(p5.getExpDate());
		p5.setExpDate(4, 31, 2021);
		System.out.println(p5.getName());
		System.out.println(p5.getPrice());
		System.out.println(p5.getExpDate());
		if (p5.getExpDate() == null) {
			System.out.println("Invalid date");
		}
		System.out.print(p5.toCSV());
		System.out.println();
		//WORKS (invalid day is not set)
		
		
		System.out.println(p.toString());
		System.out.println(p5.toString());
		System.out.println(p.toCSV());
		System.out.println(p5.toCSV());
		System.out.println();
		
		//TESTING AGERESTRICTED ITEMS
		AgeRestricted ag = new AgeRestricted();
		ag.setName("cigarettes");
		System.out.println(ag.getName());
		ag.setPrice(14.00);
		System.out.println(ag.getPrice());
		System.out.println(ag.getAgeLimit());
		System.out.println(ag.toCSV());
		System.out.println();
		
		//TESTING SHELVED ITEMS
		Shelved shelved = new Shelved();
		shelved.setName("pasta");
		System.out.println(shelved.getName());
		shelved.setPrice(3.99);
		System.out.println(shelved.getPrice());
		System.out.println(shelved.toCSV());
		System.out.println();
		
		//TESTING ITEM
		Item item = new Item();
		
		//TESTING TO CSV
		System.out.println(item.toCSV());
		System.out.println(p.toCSV());
		System.out.println(ag.toCSV());
		System.out.println(shelved.toCSV());
		System.out.println();
		
		//TESTING TOSTRING
		System.out.println(item.toString());
		System.out.println(p.toString());
		System.out.println(ag.toString());
		System.out.println(shelved.toString());
		System.out.println();
		
		//TEST ARRAYLIST
		ArrayList<Item> items = new ArrayList<Item>(10);
		items.add(p2);
		items.add(ag);
		items.add(shelved);
		for (Item item1 : items) {
			System.out.println(item1.toString());
		}
		System.out.println();
		
		System.out.println(p.toCSV());
		System.out.println(p2.toCSV());
		System.out.println(p3.toCSV());
		System.out.println(p4.toCSV());
		System.out.println(p5.toCSV());
		System.out.println(ag.toCSV());
		System.out.println(shelved.toCSV());
		System.out.println(item.toCSV());
		
	}


}
