package project;

public class Shelved extends Item {

	//CHARLES PAGUIA
	//4/27/2020
	//SHELVED: INHERITS FROM ITEM CLASS. NO EXPIRATION DATE. NO AGE LIMIT.
	//Slightly redundant class but makes everything more organized
	
	//DEFAULT CONSTRUCTOR
	public Shelved() {
		super();
	}
	
	//CONSTRUCTOR
	public Shelved(String name, double price) {
		super(name, price);
	}
	
	//SHELVED TOSTRING. OVERRIDES ITEM TOSTRING.
	@Override
	public String toString() {
		return super.toString();
	}
	
	//SHELVED EQUALS. OVERRIDES ITEM EQUALS.
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) { //check name and price
			return false;
		} else if (!(obj instanceof Shelved)) { //check if produce
			return false;
		}
		//if everything matches, item is equal to object obj
		return true;
	}
	
	//SHELVED TOCSV. OVERRIDES ITEM TOCSV. USED IN WRITING TO FILE.
	@Override
	public String toCSV() {
		return super.toCSV();
	}
	
}
