package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class StoreApp extends Store {
	
	//CHARLES PAGUIA
	//4/27/2020
	//STOREAPP: CLIENT FILE TO STORE. READS FILES, WRITES FILES, PROMPTS USER FOR OPTIONS.

	//READS FILE AND ADDS ITEMS TO STOCK.
	public static ArrayList<Item> readFile(String filename) throws IOException {
		//Create default stock array
		ArrayList<Item> stock = new ArrayList<Item>(50);
		//Import file
		try {
			File file = new File(filename);
			Scanner input = new Scanner(file);
			
			while (input.hasNextLine()) {
				try { //Catch file reading errors
					String line = input.nextLine();
					String[] values = line.split(",", 3); //Split line into name, price, ageLimit/expDate
					if (values.length == 3) { //It must be produce or age-restricted					
						String[] subvalues = values[2].split("/", 3); //Try splitting last index to see if its a date or age
						if (subvalues.length == 3) { // If its a date (has month day and year) it must be produce
							Produce produce = new Produce();
							produce.setName(values[0]);
							produce.setPrice(Double.parseDouble(values[1]));
							produce.setExpDate(Integer.parseInt(subvalues[0]), Integer.parseInt(subvalues[1]), Integer.parseInt(subvalues[2]));
							if (produce.getExpDate() != null) {
								stock.add(produce);
								// System.out.println("Added " + produce.toString());
							} else {
								// System.out.println("Invalid item expiration date");
							}
						} else if (subvalues.length == 1) { //It must be an age limit, thus its age-restricted
							AgeRestricted agerestricted = new AgeRestricted(
							values[0],
							Double.parseDouble(values[1]),
							Integer.parseInt(values[2]));
							stock.add(agerestricted);
							// System.out.println("Added " + agerestricted.toString());
						} else {
							// System.out.println("Error reading in item");
						}
					} else if (values.length == 2) { //It must be shelved
						Shelved shelved = new Shelved(
								values[0],
								Double.parseDouble(values[1]));
						stock.add(shelved);
						// System.out.println("Added " + shelved.toString());
					} else {
						// System.out.println("Error reading item.");
					}
				} catch (Exception e) {
					System.out.println("Error reading file.");
				}
			}
			input.close();
		} catch (FileNotFoundException fnf) {
			//nothing
		} catch (Exception e) {
			System.out.println("Error reading file.");
		}
		return stock;
	}
	
	//RUNS MENU. PROMPTS USERS FOR OPTIONS
	public static ArrayList<Item> menu(ArrayList<Item> stock) {
		//Initialize do-while flag
		boolean running = true;
		Scanner input = new Scanner(System.in);
		do {
			System.out.print(
			"\nHello. What would you like to do today?\n" +
			"1. Add item\n" +
			"2. Shop\n" +
			"3. Search\n" +
			"4. Modify item\n" +
			"5. List stock\n" +
			"6. Save & Exit\n" + "Choice: ");
			int choice = 0;
			try {
				choice = Integer.parseInt(input.nextLine());
				switch (choice) {
					case 1:
						//Add item to store
						stock = addToStock(stock);
						break;
					case 2:
						//Buy from stock
						stock = buyStock(stock);
						break;
					case 3:
						//Search for item
						System.out.println("\nOption 3. Search for store item");
						searchFor(stock);
						break;
					case 4:
						//Search for item then modify
						Item found = searchFor(stock);
						if (found != null) {
							stock = modifyItem(stock, found);
						} else {
							System.out.println("No item to modify.");
						}
						break;
					case 5:
						//List stock
						System.out.println("\nOption 5. List stock");
						listItems(stock);
						break;
					case 6:
						//Exit menu
						running = false;
						break;
					default:
						System.out.println("Invalid entry.");
						break;
				}
			} catch (NumberFormatException nfe) {
				System.out.println("Invalid entry.");
			} catch (Exception e) {
				System.out.println(e);
			}
		} while (running);
		
		return stock;
	}

	//OPTION 1 FROM MENU. ASKS IF USERS WANT TO CREATE OR DUPLICATE AN ITEM IN STOCK, THEN ADDS IT TO STOCK.
	public static ArrayList<Item> addToStock(ArrayList<Item> stock) {
		Scanner input = new Scanner(System.in);
		//prompts item, then adds item to stock
		System.out.print("\nOption 1: Add item to the store\n" +
			"1. Clone existing item\n" +
			"2. Create new item\nChoice: ");
		try {
			int choice1 = Integer.parseInt(input.nextLine());
			switch (choice1) {
			case 1:
				//search for and clone item
				Item item = searchFor(stock);
				if (item != null) {
					try {
					System.out.println("Clone item (y/n)?");
					String choice2 = input.nextLine();
					switch (choice2) {
						case "yes":
						case "y":
							Item clonedItem = cloneItem(item);
							stock.add(clonedItem);
							System.out.println("Cloned " + item.getName() + ".");
							break;
						case "no":
						case "n":
							System.out.println("Returning to main menu.");
							break;
						}
					} catch (Exception e) {
						System.out.println("Error.");
					}
				}
				break;
			case 2:
				//create stock
				stock = createStock(stock);
				break;
			default:
				System.out.println("Invalid entry.");
				break;
			}
		} catch (Exception e) {
			System.out.println("Error.");
		}
		return stock;
	}

	//UTILIZES METHODS FROM STORE.JAVA TO CREATE ITEM AND ADD IT TO STOCK.
	private static ArrayList<Item> createStock(ArrayList<Item> stock) {
		Scanner input = new Scanner(System.in);
		System.out.print(
				"Item type:\n" +
				"1. Shelved\n" +
				"2. Produce\n" +
				"3. Age Restricted\nChoice: ");
			try {
				int choice = Integer.parseInt(input.nextLine());
				switch (choice) {
					case 1:
						Shelved shelved = createShelved(input);					
						//add shelved to stock
						stock.add(shelved);
						System.out.println("Added " + shelved.getName() + " to stock.");
						break;
					case 2:
						Produce produce = createProduce(input);
						if ((produce.getExpDate()) != null) {
							stock.add(produce);
							System.out.println("Added " + produce.getName() + " to stock.");
						} else {
							System.out.println("Invalid expiration date.");
						}
						break;
					case 3:
						AgeRestricted agerestricted = createAgeRestricted(input);					
						stock.add(agerestricted);
						System.out.println("Added " + agerestricted.getName() + " to stock.");
						break;
					default:
						System.out.println("Invalid choice");
						break;
				}
				
			} catch (NumberFormatException nfe) {
				System.out.println("Invalid entry.");
			} catch (Exception e) {
				System.out.println("Error.");
			}
		return stock;
	}

	//OPTION 2 FROM MENU. CREATES GROCERT CART FOR SESSION AND PROMPTS USERS TO SHOP FROM STORE.
	//HANDLES TWO SEPARATE ARRAYLISTS OF STOCK AND CART TO MAKE SURE NO ITEMS GO MISSING.
	//	i.E. IF USER ADDS ITEMS TO CART AND RETURNS TO MAIN MENU THOSE ITEMS MUST BE PUT BACK INTO STOCK.
	//DISPLAYS CART. ADDS TO CART. REMOVES FROM CART. CLEARTS CART. AND CHECKS OUT CART.
	private static ArrayList<Item> buyStock(ArrayList<Item> stock) {
		Scanner input = new Scanner(System.in);
		boolean running = true;
		//do while menu loop
		//create grocery cart for current buying session
		ArrayList<Item> cart = new ArrayList<Item>(10);
		do {
			System.out.print("\nOption 2: Buy item from store\n" +
				"1. Display grocery cart\n" +
				"2. Add item to cart\n" +
				"3. Remove item from cart\n" +
				"4. Clear cart\n" +
				"5. Check out cart\n" +
				"6. Back to main menu\nChoice: ");
			try {
				int choice = Integer.parseInt(input.nextLine());
				switch (choice) {
					case 1://display grocery cart
						System.out.println("\nOption 1. Display cart");
						listItems(cart);			
						break;
					case 2: //search for, buy item, and add to cart
						System.out.println("\nOption 2. Add to cart");
						Item item;
						item = searchFor(stock);
						if (item != null) { //if item is found
							if (item instanceof AgeRestricted) {
								int ageLimit = ((AgeRestricted)item).getAgeLimit();
								System.out.println("\nYou must be " + ageLimit + " years old to purchase this item.");
								System.out.print("Age: ");
								int userAge;
								try {
									userAge = Integer.parseInt(input.nextLine());
									if (userAge >= ageLimit) {
										System.out.println("You are old enough to buy this item.");
										System.out.println("Adding " + item.getName() + " to cart.");
										cart.add(item); //add item to cart
										stock.remove(item); //remove item from stock
									} else {
										System.out.println("You cannot buy this item.");
									}
								} catch (NumberFormatException e) {
									System.out.println("Invalid entry. You cannot buy this item.");
								} catch (Exception e) {
									System.out.println("Error.");
								}
							} else {
								System.out.println("Adding " + item.getName() + " to cart.");
								cart.add(item); //add item to cart
								stock.remove(item); //remove item from stock
							}
							
						}
						break;
					case 3: //search for item in cart, remove from cart
						System.out.println("\nOption 3. Remove from cart");
						item = searchFor(cart);
						if (item != null) { //if item is found
							System.out.println("Removing " + item.getName() + " from cart.");
							stock.add(item); //add item to cart
							cart.remove(item); //remove item from stock
						}
						break;
					case 4:
						System.out.println("\nOption 4. Clear cart");
						//reaffirm you will clear the cart
						System.out.println("Are you sure you want to clear cart (y/n)?");
						String ynPrompt1 = input.nextLine().toLowerCase();
						switch (ynPrompt1) {
							case "yes":
							case "y":
								//put cart items back in store stock
								for (Item i : cart) {
									stock.add(i);
								}
								cart.clear();
								System.out.println("Cart cleared. Items returned to store.");
								break;
							case "no":
							case "n":
								//don't clear cart
								System.out.println("Cart will not be cleared.");
								break;
							default:
								System.out.println("Invalid entry.");
								break;
						}
						break;
					case 5:
						System.out.println("\nOption 5. Check out");
						System.out.print("\nAre you sure you want to check out (y/n)?\nChoice:");
						String ynPrompt2 = input.nextLine().toLowerCase();
						switch (ynPrompt2) {
							case "yes":
							case "y":
								double totalCost = 0.0; //find total cost. empty cart. print total cost.
								for (Item i : cart) {
									totalCost += i.getPrice();
								}
								cart.clear();
								System.out.println("\nClearing cart...");
								System.out.println("Calculating cost...");
								System.out.println("Total Cost: $" + String.format("%.02f", totalCost) + ".\n");
								running = false;
								break;
							case "no":
							case "n":
								//don't clear cart
								System.out.println("Cart will not be cleared.");
								break;
							default:
								System.out.println("Invalid entry.");
								break;
						}
						break;
					case 6:
						System.out.println("\nOption 6. Exit");
						System.out.println("Returning to main menu.");
						//Return cart items to stock
						if (!cart.isEmpty()) {
							for (Item i : cart) {
								stock.add(i);
							}
						}
						running = false;
						break;
					default:
						System.out.println("Invalid entry.");
						break;
				}
			} catch (NumberFormatException e) {
				System.out.println("Invalid entry.");
			} catch (Exception e) {
				System.out.println("Error.");
			}
		} while (running);
		return stock;
	}

	//OPTION 3 FROM MENU. ALLOWS USER TO SEARCH FOR ITEM FROM ARRAYLIST<ITEM>
	//UTILIZED IN OTHER METHODS TO MODIFY, ADD, OR REMOVE ITEMS FROM CART OR STOCK DEPENDING ON SCENARIO.
	public static Item searchFor(ArrayList<Item> itemlist) {
		Scanner input = new Scanner(System.in);
		//prompts for searchable
		System.out.print("Search for: ");
		//initializes local vars
		ArrayList<Item> foundItems = new ArrayList<Item>(5);
		try {
			String searchable = input.nextLine();
			System.out.println("\nSearching...");
			int amtFound = 0;
			//finds and lists item(s)
			for (Item thing : itemlist) {
				if (thing.getName().equalsIgnoreCase(searchable)) { //if name matches
					foundItems.add(thing); //add to found items list
					amtFound++; //increase amount found
				}
			}
			if (amtFound > 0) { //if items found
				System.out.println("Found " + amtFound + " instance(s) of " + searchable + "."); //say how many items it found
				for (Item founds : foundItems) {
				System.out.println(founds.toString()); //list items found
				}
			} else { //if no items found
				System.out.println("No items found..."); //say no items found
			}
		} catch (Exception e) {
			System.out.println("Error searching.");
		}
		if (!foundItems.isEmpty()) {
			//return a single item
			return foundItems.get(0);
		}
		return null;
		
	}

	//OPTION 4 FROM MENU. UTILIZES SEARCHFOR AND CREATESTOCK METHODS TO CREATE AND REPLACE REQUESTED ITEM.
	private static ArrayList<Item> modifyItem(ArrayList<Item> stock, Item toModify) {
		System.out.println("\nOption 4. Modify item");
		Scanner input = new Scanner(System.in);
		//Find index of item toModify
		int index = stock.indexOf(toModify);
		//Ask what type of item you want to turn it into
		System.out.print("Item type:" +
				"\n1. Shelved" +
				"\n2. Produce" +
				"\n3. Age Restricted" +
				"\nChoice: ");
		try {
			int itemType = Integer.parseInt(input.nextLine());
			switch (itemType) {
				case 1:
					Shelved modified1 = createShelved(input); // Create new item and replace toModify item
					stock.set(index, modified1); // Replace item with new modified item
					System.out.println(toModify.getName() + " --> " + modified1.getName() + ".");
					break;
				case 2:
					Produce modified2 = createProduce(input);
					stock.set(index, modified2);
					System.out.println(toModify.getName() + " --> " + modified2.getName() + ".");
					break;
				case 3:
					AgeRestricted modified3 = createAgeRestricted(input);
					stock.set(index, modified3);
					System.out.println(toModify.getName() + " --> " + modified3.getName() + ".");
					break;
				default:
					break;
			}
		} catch (NumberFormatException nfe) {
			System.out.println("Invalid entry.");
		} catch (Exception e) {
			System.out.println("Error.");
		}
		return stock;
		
	}

	//OPTION 5 FROM MENU. ITERATES OVER STOCK TO LIST EACH ITEM IN CART OR STOCK DEPENDING ON SCENARIO.
	//ALSO UTILIZED IN BUYSTOCK MENU TO DISPLAY CURRENT SESSIONS GROCERY CART.
	private static void listItems(ArrayList<Item> itemlist) {
		if (!itemlist.isEmpty()) {
			System.out.println("Displaying items:");
			for (Item i : itemlist) {
			System.out.println(i.toString());
			}
		} else {
			System.out.println("No items to display.");
		}	
	}

	//SAVES FILE. TAKES ITEMS FROM STOCK AND WRITES THEM TO FILENAME.
	private static void saveFile(String filename, ArrayList<Item> stock) {
		try {
			FileWriter writer = new FileWriter(filename); //Create FileWriter object
			for (Item item : stock) {
				writer.write(item.toCSV() + "\n"); //Write to file
				writer.flush(); //Flush in case
			}
			writer.close();
		} catch (Exception e) { //Catch errors
			System.out.println("Error saving file.");
		}
	}
	
	//PROMPTS USER FOR FILENAME TO READ. RUNS MENU. WRITES TO FILE. ENDS PROGRAM.
	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: ");
		String filename = "src/project/" + input.nextLine() + ".csv";
		//Read File
		ArrayList<Item> stock = readFile(filename);
		//Menu
		stock = menu(stock);
		//Save
		saveFile(filename, stock);
		//Close
		input.close();
		System.out.println("Goodbye.");
		
	}
	
}
