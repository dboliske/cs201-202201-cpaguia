Project Design I
Charles Paguia

1. Describe the user interface. What are the menu options and how will the user use the application?

	User interface will be a do while loop as a static menu method prompting the user for to 1. create an item
	2. sell an item 3. search for an item 4. change item price or name and 5. exit
	
	It will look like this
	
	Enter Filename: (src/project/store.csv)
	//if file found
	File found.
	//if file not found
	File not found.	
	
	Hello. What would you like to do today?
	1. Add item to store
	2. Buy item from store
	3. Search for store item
	4. Update item price/name
	5. Exit
	
----If option 1 is selected it will look like:
	
	Option 1: Add item to the store.
	What type of item is it? (produce, shelved, or age-restricted?
	
	
	(If age restricted is selected ask:)
	What age is the age restriction?: 
	(else):
	
	What is the name of the item you wish to add?: 
	What is the price of the item?: 
	
	[item name] costs [item price] and is in the [produce section, shelved, age-restricted section].
	
----If option 2 is selected it will look like:
	
	Option 2: Buy item from store
	1. Display grocery cart
	2. Buy item
	3. Back to main menu
	
	(if check grocery cart):
	In grocery cart:
	[2] [banana]s
	[1] [cheerios]
	[4] [bean can]s	
	(if buy item):
	Which item would you like to buy?: 
	(ex) bananas
	Searching for [item].
	We have [amount] in stock. (if more than one) How many would you like to buy?
	(ex) 2
	(if over possible amount)
	Not enough in stock.
	(if anything but a positive integer)
	Unexpected user entry.
	(if possible amount)
	[2] [banana]s are now in your grocery cart.
	
----If option 3 is selected it will look like:

	Which item would you like to search for?: (ex) apple
	//if not found
	[apple] not found.
	//if found
	We have [3] [apple]s in stock for [item price] each.
	
----If option 4 is selected it will look like:

	Which item would you like to update?: 
	//search for item to update
	[apple] found.
	(or)
	[apple] not found.
	What would you like to change the name to?: 
	What would you like to change the price to?:
	[apple] costing [item price] is now [orange] costing [new item price].

----If option 5 is selected it will look like:

	Saving file...
	Store closing.
	
	
2. 2.Describe the programmers' tasks:
	a.Describe how you will read the input file.
	b.Describe how you will process the data from the input file.
	c.Describe how you will store the data (what objects will you store?)
	d.How will you add/delete/modify data?
	e.How will you search the data?
	f.List the classes you will need to implement your application.
	
	a. How will you read the input file?
		Data will be written like:
		banana,1.49,produce
		banana,1.49,produce
		cigarettes,10.50,ageRestricted
		beans,3.40,shelved
		
		So when it's read it will use the subString method to divide the string by the commas
		and parse the data by name:String price:Float type:String. the type will then be converted
		to the appropriate type depending on if it says "produce" "ageRestricted" or "shelved."
		Each item will be stored as an instance of class Item in ArrayList<Item>
	
	b. How will you process the input file?
		Process item, add to ArrayList<Item>.
		ArrayList<Item> will then be written to the save file as described above
		if Item instanceof Produce
			write produce
		if Item instanceof ageRestricted
			write ageRestricted
		if Item instanceof Shelved
			write shelved
	
	c. How will you store the data?
		Each item will be stored as an instance of class Item in ArrayList<Item>
		
	d. How will you add/delete/modify data?
		Data will be stored in an ArrayList<Item>
		Data will be created as an instance of Item and then stored in the array list
		Data will be deleted using the array list remove(int index) method
		Data will be modified using the array list add(int index, E element) method		
		The ArrayList will be written into a .csv save file before program exits
	
	e. How will you search the data?
		Data will be searched using search methods
	
	f. List the classes you will need to implement your application.
		I will need an Item Class.
		I will need a Produce, Shelved, and AgeRestricted class which inherits from the Item Class
		
3. Draw UML Diagram
	
	In package listed as StoreUML.png	
	
4.  Think how you will determine if your code functions are expected.
Develop a test plan based on the above description; 
how will you test that the expected outputs have been achieved for every menu option?
	
	Menu test code written in above in question 1.
	
	
	
	
		
	
	
	
	
	
	