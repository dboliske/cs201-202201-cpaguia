package project;

public class Item {

	//CHARLES PAGUIA
	//4/27/2020
	//ITEM: IMPLEMENTS ITEM CLASS WHICH SHELVED, PRODUCE, AND AGERESTRICTED ITEMS ALL INHERIT FROM
	
	private String name;
	private double price;
	
	//DEFAULT CONSTRUCTOR
	public Item() {
		this.name = "item";
		this.price = 1.00;
	}
	
	//CONSTRUCTOR. INITIALIZES AND SETS NAME AND PRICE.
	public Item(String name, double price) {
		this.name = "item";
		setName(name);
		this.price = 1.00;
		setPrice(price);
	}
	
	//NAME GETTER
	public String getName() {
		return name;
	}
	
	//PRICE GETTER
	public double getPrice() {
		return price;
	}
	
	//NAME SETTER
	public void setName(String name) {
		this.name = name;
	}
	
	//PRICE SETTER
	public void setPrice(double price) {
		if (price >= 0) {
			this.price = price;
		}		
	}

	//ITEM TOSTRING. OTHER CLASSES INHERIT FROM THIS.
	@Override
	public String toString() {
		return "Item: " + name + ". Price: $" + String.format("%.02f", price);
	}
	
	//ITEM EQUALS. OTHER CLASSES INHERIT FROM THIS.
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Item)) {
			return false;
		}
		//cast obj into item
		Item i = (Item)obj;
		if (!this.name.equalsIgnoreCase(i.getName())) {
			return false;
		} else if (this.price != i.getPrice()) {
			return false;
		}
		//if everything matches, item is equal to object obj
		return true;
	}
	
	//ITEM TOCSV. OTHER CLASSES INHERIT FROM THIS. USED IN WRITING TO FILE.
	protected String toCSV() {
		return name + "," + String.format("%.02f", price);
	}
	
}
